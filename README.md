# Show Time

## Índice

- [Problema](#Problema)
    - [Meli shows API](#Meli shows API)
    - [Requisitos funcionales](#Requisitos Funcionales)
- [Solución](#Solución)
    - [Tecnologías utilizadas](#Tecnologías utilizadas)
    - [Dependencias](#Dependencias)
    - [Diagramas](#Diagramas)
        - [Arquitectura](#Arquitectura)
        - [Componentes de alto nivel](#Componentes de alto nivel)
    - [Endpoints](#Endpoints)
    - [Despliegues recomendados](#Despliegues recomendados)
- [Bibliografía](#Bibliografía)

## Problema

BIENVENIDO AL SHOW BUSINESS! Estás incursionando en el mercado del espectáculo brindando una API que permita a
diferentes portales la venta de tickets para shows artísticos (actuación, música, etc.) concentrando la oferta de
diferentes teatros, sus shows y pudiendo realizar la reserva de la o las localidades para una función del show.

### Meli shows API

Nuestra API tendrá dos usuarios directos:

1. Quién alimenta de información la plataforma agregando los teatros con sus correspondientes salas y butacas.
2. El portal (que nos utilizara para hacer una reserva en nombre de un espectador).

El primero, aparte de dar de alta los teatros, sus salas y butacas, también carga los shows y funciones disponibles (
especificando en qué sala de cada teatro, será cada función); además de los precios de las butacas para cada sección (
grupo de butacas) de la sala en cuestión.

*Nota: Una obra puede decidir tener diferentes precios para diferentes secciones o puede directamente decidir cobrar lo
mismo para cualquier ubicación. También es posible que para una misma sala, dos obras diferentes tengan distintas
secciones o como mencionamos, una sola.*

El otro usuario será el portal, quien mediante su aplicación nos utilizará para hacer la reserva en nombre del
espectador (su cliente final). Para esto la API deberá poder disponer de la siguiente funcionalidad (que es la te
pedimos que implementes):

* Poder listar los shows y funciones disponibles con todas sus características.
* Para una función de un show deberá ser posible listar las butacas disponibles con el precio de las mismas.
* Y por último deberá poderse realizar una reserva para la función de un show, indicando DNI, nombre del espectador y
  las butacas con el precio registrando todo a modo de Ticket de reserva.

*Nota: Toma en cuenta la posibilidad de que 2 clientes vean disponible la misma localidad, uno haga la reserva y después
el segundo cliente intente también reservar. La API debería no dejar al segundo realizar la reserva si alguna de las
localidades que solicita ya fue reservada.*

Todo lo referente a la carga de datos puede ser ignorado y cargado del origen que elijas (archivo, resource, hardcoded,
etc.). No es necesario que la API exponga esta funcionalidad (podés ignorar los endpoints para hacer la carga de la
información de shows, teatro, salas, etc.), más sí que al iniciar la información la obtenga de algún lado. En resumen
tiene que tener datos para poder utilizarla, pero no es necesario que estén expuestos los endpoints para hacerlo via la
API de modo de simplificar el problema.

### Requisitos Funcionales

Te pedimos:

1. Implementar la API con la funcionalidad solicitada respetando los principios RESTful.
2. Agregale a la API la posibilidad de realizar búsquedas de shows con criterios complejos como: todos los shows que
   tengan funciones programadas entre la fecha X y la fecha Y, que su precio esté dentro del rango de precios Z y K y
   pudiendo solicitar que los resultados esten ordenados de forma ascendente o descendente según algún atributo del
   show.
3. Implementar una forma de asegurar que la información solicitada más frecuentemente no se busque en el repositorio de
   datos, sino que esté disponible de una forma más eficiente.
4. Hostear la API en un cloud computing libre (Google App Engine, Amazon AWS, etc).
5. Documenta con diagramas de arquitectura que expliquen la solucion y que contengan los componentes de tu diseño y
   deployment recomendado para ir cumpliendo los hitos
   (iteraciones de la evolución del enunciado)

## Solución

Para resolver el problema enunciado se eligió Java como lenguaje, así como Spring Boot como framework. Se diseñó una
arquitectura limpia (Clean Architecture), específicamente una versión libre de arquitectura hexagonal.

### Tecnologías utilizadas

* [Spring-Boot 2.6.3.][spring]
* [Java 11.][java]
* [Gradle 7.4][gradle]

### Dependencias

* spring-boot-starter-web
* spring-boot-starter-security
* spring-boot-starter-data-jdbc
* spring-boot-starter-test
* spring-security-test
* h2:2.1.210
* lombok:1.18.22
* springdoc-openapi-ui:1.6.6'
* archunit:0.22.0

### Diagramas

#### Arquitectura

Se siguió el estilo arquitectónico propuesto por Robert Martin (a.k.a Uncle Bob) en su libro: *Clean Architectures: A
Code of Conduct for Professional Programmers*. Más específicamente una interpretación libre de la arquitectura
hexagonal, propuesta por Alistar Cockburn (también conocida como *Ports and Adapters*).

|      Clean Architecture      |          Hexagonal          |
|:----------------------------:|:---------------------------:|
| ![](diagrams/clean-arch.png) | ![](diagrams/hexa-arch.png) |

<p style="text-align: center"> Diagramas de arquitectura </p>

#### Componentes de alto nivel

Como ya se mencionó en la sección donde se definieron las [tecnologías](#Tecnologías utilizadas), el microservicio se
desarrolló utilizando Spring Boot.

Si bien tanto la base de datos (H2) como la caché (In memory) utilizadas son ambas embebidas, para ambientes productivos
se recomienda tener una base y una caché como componentes totalmente independientes de la ejecución del servicio (Neo4J,
Postgres, Mongo, Redis, etc.).

|      Componentes de alto nivel       |
|:------------------------------------:|
| ![](diagrams/components-diagram.png) |

### Endpoints

Los endpoints se pueden encontrar entrando al [swagger](https://shows-booking.rj.r.appspot.com/swagger-ui.html) que
hostea el microservicio, que a su vez se encuentra desplegado en los servicios de google cloud.

* POST ⇒ /api/v1/bookings: Realiza una reserva a nombre de un cliente, de uno o varios asientos para una función en
  específico.
* GET ⇒ /api/v1/theaters: Lista los teatros disponibles (así como sus shows y funciones disponibles)
* GET ⇒ /api/v1/shows: Lista los shows con sus respectivas funciones y asientos disponibles.
* GET ⇒ /api/v1/show/{id}: Muestra los detalles de un show especificado, con sus respectivas funciones y asientos
  disponibles

### Despliegues recomendados

En aras de cumplir los hitos de la forma que aporte el mayor valor posible al negocio, recomienda que las iteraciones (
deployments) sean las siguientes en ese orden:

1. Creación de una reserva (persistencia + seguridad).
2. Listar shows y sus funciones, así como los asientos disponibles.
3. Agregar filtrado según criterios.
4. Agregar caché para agilizar consultas.

## Bibliografía

* https://medium.com/@lars.willemsens/using-gitlab-ci-cd-to-deploy-a-spring-boot-application-in-google-cloud-3b22474e3ffc
* https://www.baeldung.com/spring-boot-h2-database

[spring]:https://spring.io/projects/spring-boot

[gradle]:https://docs.gradle.org/current/userguide/userguide.html

[java]:https://www.oracle.com/java/technologies/javase/jdk11-archive-downloads.html
