SELECT chair.id, section_id, section.price
FROM chair
         LEFT JOIN section ON section.id = chair.section_id
WHERE chair.id = :id;