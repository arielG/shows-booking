SELECT chair.id, performance_id
FROM chair
         LEFT JOIN chair_performance ON chair_performance.chair_id = chair.id
         LEFT JOIN performance ON performance.id = chair_performance.performance_id
WHERE performance.id = :performance_id