-- Creating theaters
INSERT INTO theater (id, name)
VALUES (1, 'Gran Teatro de La Habana');
INSERT INTO theater (id, name)
VALUES (2, 'Teatro Colón');

-- Creating rooms
INSERT INTO room (id, theater_id)
VALUES (1, 1);
INSERT INTO room (id, theater_id)
VALUES (2, 1);
INSERT INTO room (id, theater_id)
VALUES (3, 2);
INSERT INTO room (id, theater_id)
VALUES (4, 2);

-- Creating sections
INSERT INTO section (id, price, room_id)
VALUES (1, null, 1);
INSERT INTO section (id, price, room_id)
VALUES (2, null, 1);
INSERT INTO section (id, price, room_id)
VALUES (3, 100, 2);
INSERT INTO section (id, price, room_id)
VALUES (4, 350, 2);
INSERT INTO section (id, price, room_id)
VALUES (5, null, 3);
INSERT INTO section (id, price, room_id)
VALUES (6, 900, 3);
INSERT INTO section (id, price, room_id)
VALUES (7, 100, 4);
INSERT INTO section (id, price, room_id)
VALUES (8, 50, 4);

--Creating chairs
INSERT INTO chair (id, section_id)
VALUES (1, 1);
INSERT INTO chair (id, section_id)
VALUES (2, 1);
INSERT INTO chair (id, section_id)
VALUES (3, 1);
INSERT INTO chair (id, section_id)
VALUES (4, 1);
INSERT INTO chair (id, section_id)
VALUES (5, 2);
INSERT INTO chair (id, section_id)
VALUES (6, 2);
INSERT INTO chair (id, section_id)
VALUES (7, 2);
INSERT INTO chair (id, section_id)
VALUES (8, 2);
INSERT INTO chair (id, section_id)
VALUES (9, 3);
INSERT INTO chair (id, section_id)
VALUES (10, 3);
INSERT INTO chair (id, section_id)
VALUES (11, 3);
INSERT INTO chair (id, section_id)
VALUES (12, 3);
INSERT INTO chair (id, section_id)
VALUES (13, 4);
INSERT INTO chair (id, section_id)
VALUES (14, 4);
INSERT INTO chair (id, section_id)
VALUES (15, 4);
INSERT INTO chair (id, section_id)
VALUES (16, 4);
INSERT INTO chair (id, section_id)
VALUES (17, 5);
INSERT INTO chair (id, section_id)
VALUES (18, 5);
INSERT INTO chair (id, section_id)
VALUES (19, 5);
INSERT INTO chair (id, section_id)
VALUES (20, 5);
INSERT INTO chair (id, section_id)
VALUES (21, 6);
INSERT INTO chair (id, section_id)
VALUES (22, 6);
INSERT INTO chair (id, section_id)
VALUES (23, 6);
INSERT INTO chair (id, section_id)
VALUES (24, 6);
INSERT INTO chair (id, section_id)
VALUES (25, 7);
INSERT INTO chair (id, section_id)
VALUES (26, 7);
INSERT INTO chair (id, section_id)
VALUES (27, 7);
INSERT INTO chair (id, section_id)
VALUES (28, 7);
INSERT INTO chair (id, section_id)
VALUES (29, 8);
INSERT INTO chair (id, section_id)
VALUES (30, 8);
INSERT INTO chair (id, section_id)
VALUES (31, 8);
INSERT INTO chair (id, section_id)
VALUES (32, 8);

--Creating client
INSERT INTO client (dni, name)
VALUES ('95799766', 'randomClient');

--Creating show
INSERT INTO show (id, name)
VALUES (1, 'The amazing normal man');
INSERT INTO show (id, name)
VALUES (2, 'The amazing normal woman');

--Creating performance
INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (1, '2022-02-28 20:00', 100, 1, 1);
INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (2, '2022-03-14 22:00', 200, 2, 1);

INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (3, '2022-03-20 21:00', 50, 1, 2);
INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (4, '2022-04-10 20:00', 300, 2, 2);

INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (5, '2022-02-16 16:00', 450, 1, 3);
INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (6, '2022-03-25 17:00', 1000, 2, 3);

INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (7, '2022-05-01 23:00', 70, 1, 4);
INSERT INTO performance (id, date, price, show_id, room_id)
VALUES (8, '2022-06-13 18:00', 45, 2, 4);