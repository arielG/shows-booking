CREATE TABLE IF NOT EXISTS theater
(
    id   INTEGER NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS room
(
    id         INTEGER NOT NULL,
    theater_id INTEGER NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT room_fk
        FOREIGN KEY (theater_id) REFERENCES theater (id)
);

CREATE TABLE IF NOT EXISTS section
(
    id      INTEGER NOT NULL,
    price   NUMERIC(10, 2),
    room_id INTEGER NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT section_fk
        FOREIGN KEY (room_id) REFERENCES room (id)
);

CREATE TABLE IF NOT EXISTS chair
(
    id         INTEGER NOT NULL,
    section_id INTEGER NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT sit_fk
        FOREIGN KEY (section_id) REFERENCES section (id)
);

CREATE TABLE IF NOT EXISTS client
(
    dni  VARCHAR NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY (dni)
);

CREATE TABLE IF NOT EXISTS show
(
    id   INTEGER NOT NULL,
    name VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS performance
(
    id      INTEGER        NOT NULL,
    date    DATETIME       NOT NULL,
    price   NUMERIC(10, 2) NOT NULL,
    show_id INTEGER        NOT NULL,
    room_id INTEGER        NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT performance_fk1
        FOREIGN KEY (show_id) REFERENCES show (id),
    CONSTRAINT performance_fk2
        FOREIGN KEY (room_id) REFERENCES room (id)
);

CREATE TABLE IF NOT EXISTS booking
(
    id             INTEGER NOT NULL AUTO_INCREMENT,
    client_id      INTEGER NOT NULL,
    performance_id INTEGER NOT NULL,
    timestamp      TIMESTAMP DEFAULT CURRENT_TIMESTAMP(),
    PRIMARY KEY (id),
    CONSTRAINT booking_fk1
        FOREIGN KEY (client_id) REFERENCES client (dni),
    CONSTRAINT booking_fk2
        FOREIGN KEY (performance_id) REFERENCES performance (id)
);

CREATE TABLE IF NOT EXISTS chair_performance
(
    chair_id       INTEGER NOT NULL,
    performance_id INTEGER NOT NULL,
    PRIMARY KEY (chair_id, performance_id),
    CONSTRAINT chair_performance_fk1
        FOREIGN KEY (chair_id) REFERENCES chair (id),
    CONSTRAINT chair_performance_fk2
        FOREIGN KEY (performance_id) REFERENCES performance (id)
);

