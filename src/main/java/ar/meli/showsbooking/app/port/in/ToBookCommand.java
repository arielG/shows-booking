package ar.meli.showsbooking.app.port.in;

import ar.meli.showsbooking.domain.Booking;

public interface ToBookCommand {

  Booking execute(Booking booking);
}
