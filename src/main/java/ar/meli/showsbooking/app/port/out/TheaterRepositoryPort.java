package ar.meli.showsbooking.app.port.out;

import ar.meli.showsbooking.domain.Theater;

import java.util.List;

public interface TheaterRepositoryPort {

  List<Theater> getAll();
}
