package ar.meli.showsbooking.app.port.in;

import ar.meli.showsbooking.domain.Theater;

import java.util.List;

public interface GetTheatersQuery {
  List<Theater> execute();
}
