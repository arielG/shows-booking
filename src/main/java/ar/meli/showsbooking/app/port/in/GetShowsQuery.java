package ar.meli.showsbooking.app.port.in;

import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.domain.filter.ShowFilter;

import java.util.List;

public interface GetShowsQuery {

  List<Show> execute(ShowFilter filter);
}
