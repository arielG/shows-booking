package ar.meli.showsbooking.app.port.out;

import ar.meli.showsbooking.domain.Show;

import java.util.List;
import java.util.Optional;

public interface ShowCachePort {

  List<Show> save(Integer key, List<Show> shows);

  Optional<List<Show>> get(Integer key);
}
