package ar.meli.showsbooking.app.port.out;

import ar.meli.showsbooking.domain.Booking;

public interface BookingRepositoryPort {

  Booking toBook(Booking booking);
}
