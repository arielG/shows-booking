package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.annotation.UseCase;
import ar.meli.showsbooking.app.port.in.GetTheatersQuery;
import ar.meli.showsbooking.app.port.out.TheaterRepositoryPort;
import ar.meli.showsbooking.domain.Theater;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@UseCase
public class GetTheaterUseCase implements GetTheatersQuery {

  private final TheaterRepositoryPort theaterRepositoryPort;

  @Autowired
  public GetTheaterUseCase(TheaterRepositoryPort theaterRepositoryPort) {
    this.theaterRepositoryPort = theaterRepositoryPort;
  }

  @Override
  public List<Theater> execute() {
    return theaterRepositoryPort.getAll();
  }
}
