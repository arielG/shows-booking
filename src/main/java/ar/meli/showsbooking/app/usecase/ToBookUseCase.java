package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.annotation.UseCase;
import ar.meli.showsbooking.app.exception.ApplicationException;
import ar.meli.showsbooking.app.exception.InfraException;
import ar.meli.showsbooking.app.port.in.ToBookCommand;
import ar.meli.showsbooking.app.port.out.BookingRepositoryPort;
import ar.meli.showsbooking.domain.Booking;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
@UseCase
public class ToBookUseCase implements ToBookCommand {

  private final BookingRepositoryPort bookingRepositoryPort;

  @Autowired
  public ToBookUseCase(BookingRepositoryPort bookingRepositoryPort) {
    this.bookingRepositoryPort = bookingRepositoryPort;
  }

  @Override
  public Booking execute(Booking booking) {
    try {
      return bookingRepositoryPort.toBook(booking);
    } catch (InfraException ex) {
      log.error("A fatal error was thrown while trying to persist the booking", ex);
      throw new ApplicationException(ErrorCode.BOOKING, ex);
    }
  }
}
