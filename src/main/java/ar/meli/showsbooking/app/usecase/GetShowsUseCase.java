package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.annotation.UseCase;
import ar.meli.showsbooking.app.exception.ApplicationException;
import ar.meli.showsbooking.app.exception.InfraException;
import ar.meli.showsbooking.app.port.in.GetShowsQuery;
import ar.meli.showsbooking.app.port.out.ShowCachePort;
import ar.meli.showsbooking.app.port.out.ShowRepositoryPort;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.domain.filter.ShowFilter;
import ar.meli.showsbooking.shared.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@UseCase
public class GetShowsUseCase implements GetShowsQuery {

  private final ShowCachePort showCachePort;
  private final ShowRepositoryPort showRepositoryPort;

  @Autowired
  public GetShowsUseCase(ShowCachePort showCachePort, ShowRepositoryPort showRepositoryPort) {
    this.showCachePort = showCachePort;
    this.showRepositoryPort = showRepositoryPort;
  }

  @Override
  public List<Show> execute(ShowFilter filter) {
    try {
      final var shows =
          this.showCachePort
              .get(filter.hashCode())
              .orElseGet(
                  () -> {
                    final var showsFromDB =
                        this.showRepositoryPort.getAll().stream()
                            .filter(filter::applyIdFilter)
                            .filter(filter::applyDateFilter)
                            .filter(filter::applyPriceFilter)
                            .collect(Collectors.toList());
                    this.showCachePort.save(filter.hashCode(), showsFromDB);
                    return showsFromDB;
                  });

      return Optional.ofNullable(filter.getSort())
          .map(order -> filter.sortShows(shows))
          .orElse(shows);
    } catch (InfraException ex) {
      throw new ApplicationException(ErrorCode.SHOWS, ex);
    }
  }
}
