package ar.meli.showsbooking.app.exception;

public class PersistenceException extends InfraException {

  public PersistenceException(String message, Throwable ex) {
    super(message, ex);
  }

  public PersistenceException(String description) {
    super(description);
  }
}
