package ar.meli.showsbooking.app.exception;

public class BusyChairException extends InfraException {

  public BusyChairException(String message, Throwable root) {
    super(message, root);
  }
}
