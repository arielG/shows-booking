package ar.meli.showsbooking.app.exception;

public class CacheException extends InfraException {
  public CacheException(String description, Exception ex) {
    super(description, ex);
  }
}
