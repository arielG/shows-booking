package ar.meli.showsbooking.app.exception;

import ar.meli.showsbooking.shared.ErrorCode;

public class ApplicationException extends RuntimeException {

  private final ErrorCode errorCode;

  public ApplicationException(ErrorCode errorCode, InfraException ex) {
    super(ex.getMessage(), ex.getCause());
    this.errorCode = errorCode;
  }

  public ErrorCode getErrorCode() {
    return errorCode;
  }
}
