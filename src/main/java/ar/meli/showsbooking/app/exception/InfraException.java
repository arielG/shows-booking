package ar.meli.showsbooking.app.exception;

public class InfraException extends RuntimeException {

  public InfraException(String message, Throwable cause) {
    super(message, cause);
  }

  public InfraException(String description) {
    super(description);
  }
}
