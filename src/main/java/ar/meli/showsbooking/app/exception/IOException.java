package ar.meli.showsbooking.app.exception;

public class IOException extends InfraException {

  public IOException(String message, Throwable ex) {
    super(message, ex);
  }

  public IOException(String description) {
    super(description);
  }
}
