package ar.meli.showsbooking.infra.cache;

import ar.meli.showsbooking.app.exception.CacheException;
import ar.meli.showsbooking.app.port.out.ShowCachePort;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.infra.cache.service.CacheService;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ShowCacheInMemo implements ShowCachePort {

  private final CacheService<Integer, List<Show>> cacheService;

  public ShowCacheInMemo(CacheService<Integer, List<Show>> cacheService) {
    this.cacheService = cacheService;
  }

  @Override
  public List<Show> save(Integer key, List<Show> shows) {
    try {
      log.info("Saving in cache shows of the filter hash {}", key);
      final var showsSaved = this.cacheService.save(key, shows);
      log.info("Shows saved correctly with key {}", key);
      return showsSaved;
    } catch (Exception ex) {
      log.error(
          "Fatal error occurred while trying to save the shows of the filter hash {}", key, ex);
      throw new CacheException(ErrorCode.CACHE_ERROR.getDescription(), ex);
    }
  }

  @Override
  public Optional<List<Show>> get(Integer key) {
    try {
      log.info("Retrieving from cache the filter with hash {}", key);
      var showsCached = this.cacheService.get(key);
      log.info("Shows retrieved from cache");
      return showsCached;
    } catch (Exception ex) {
      log.error(
          "Fatal error occurred while trying to retrieve the shows of the filter hash {}", key, ex);
      throw new CacheException(ErrorCode.CACHE_ERROR.getDescription(), ex);
    }
  }
}
