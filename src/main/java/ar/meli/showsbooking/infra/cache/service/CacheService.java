package ar.meli.showsbooking.infra.cache.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
public class CacheService<K, V> {

  private final HashMap<K, V> cache;

  public CacheService() {
    this.cache = new HashMap<>();
  }

  public V save(K key, V value) {
    return this.cache.put(key, value);
  }

  public Optional<V> get(K key) {
    return Optional.ofNullable(this.cache.get(key));
  }
}
