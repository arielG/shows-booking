package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Theater;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class TheaterDto {

  private Long id;
  private String name;
  private List<RoomDto> rooms;

  public Theater toDomain() {
    return Theater.builder()
        .id(this.id)
        .name(this.name)
        .rooms(this.rooms.stream().map(RoomDto::toDomain).collect(Collectors.toList()))
        .build();
  }
}
