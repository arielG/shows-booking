package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Room;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDto {
  @With private Long id;
  private List<SectionDto> sections;
  private List<PerformanceDto> performances;
  private TheaterDto theater;

  public Room toDomain() {
    return Room.builder()
        .id(this.id)
        .performances(
            this.performances.stream().map(PerformanceDto::toDomain).collect(Collectors.toList()))
        .build();
  }
}
