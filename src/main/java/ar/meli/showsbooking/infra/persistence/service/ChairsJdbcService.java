package ar.meli.showsbooking.infra.persistence.service;

import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.infra.persistence.model.ChairDto;
import ar.meli.showsbooking.infra.persistence.model.PerformanceDto;
import ar.meli.showsbooking.infra.persistence.model.RoomDto;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class ChairsJdbcService {

  private static final String CHAIRS_BY_ROOM_PATH = "sql/select-chairs-by-room.sql";
  private static final String CHAIRS_BY_PERFORMANCE_PATH = "sql/select-chairs-by-performance.sql";

  private final DaoService daoService;

  @Autowired
  public ChairsJdbcService(DaoService daoService) {
    this.daoService = daoService;
  }

  public List<ChairDto> findAvailable(PerformanceDto performanceDto) {
    final var roomChairs = this.findByRoom(performanceDto.getRoom());
    final var performanceChairs = this.findByPerformance(performanceDto);
    return roomChairs.stream()
        .filter(chairDto -> !performanceChairs.contains(chairDto))
        .sorted(Comparator.comparingLong(ChairDto::getId))
        .collect(Collectors.toList());
  }

  public Set<ChairDto> findByPerformance(PerformanceDto performanceDto) {
    try {
      log.info("Looking all chairs of the {} performance", performanceDto.getId());
      final var params =
          new MapSqlParameterSource().addValue("performance_id", performanceDto.getId());
      return new HashSet<>(daoService.findAll(CHAIRS_BY_PERFORMANCE_PATH, params, ChairDto.class));
    } catch (DataAccessException ex) {
      log.error(
          "Fatal error has occurred while retrieves all chairs for the performance {}",
          performanceDto.getId(),
          ex);
      throw new PersistenceException(ErrorCode.BOOKING.getDescription(), ex);
    }
  }

  private Set<ChairDto> findByRoom(RoomDto room) {
    try {
      log.info("Looking all chairs of the {} room", room.getId());
      final var params = new MapSqlParameterSource().addValue("room_id", room.getId());
      return new HashSet<>(daoService.findAll(CHAIRS_BY_ROOM_PATH, params, ChairDto.class));
    } catch (DataAccessException ex) {
      log.error(
          "Fatal error has occurred while retrieves all chairs for the room {}", room.getId(), ex);
      throw new PersistenceException(ErrorCode.BOOKING.getDescription(), ex);
    }
  }
}
