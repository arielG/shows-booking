package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Client;
import lombok.Data;

import java.util.List;

@Data
public class ClientDto {

  private String dni;
  private String name;
  private List<BookingDto> bookings;

  public static ClientDto of(Client client) {
    final var clientDto = new ClientDto();
    clientDto.setDni(client.getDni());
    clientDto.setName(client.getName());
    return clientDto;
  }

  public Client toDomain() {
    return Client.builder().dni(this.dni).name(this.name).build();
  }
}
