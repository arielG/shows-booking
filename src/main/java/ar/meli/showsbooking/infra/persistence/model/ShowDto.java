package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Show;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class ShowDto {

  private Long id;
  private String name;
  private List<PerformanceDto> performances;

  public Show toDomain() {
    return Show.builder()
        .id(this.id)
        .name(this.name)
        .performances(
            this.performances.stream().map(PerformanceDto::toDomain).collect(Collectors.toList()))
        .build();
  }
}
