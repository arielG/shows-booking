package ar.meli.showsbooking.infra.persistence.service;

import ar.meli.showsbooking.app.exception.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;
import java.util.Optional;

import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Repository
public class DaoService {

  private final NamedParameterJdbcTemplate jdbcTemplate;

  public DaoService(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  public <T> Optional<T> findBy(String query, SqlParameterSource params, Class<T> clazz) {
    log.debug("Looking for all {} in the database with query {}", clazz.getCanonicalName(), query);
    return this.jdbcTemplate
        .query(this.loadQuery(query), params, new BeanPropertyRowMapper<>(clazz))
        .stream()
        .findFirst();
  }

  public <T> List<T> findAll(String query, Class<T> clazz) {
    log.debug("Looking for all {} in the database with query {}", clazz.getCanonicalName(), query);
    return this.jdbcTemplate.query(this.loadQuery(query), new BeanPropertyRowMapper<>(clazz));
  }

  public <T> List<T> findAll(String query, SqlParameterSource params, Class<T> clazz) {
    log.debug("Looking for all {} in the database with query {}", clazz.getCanonicalName(), query);
    return this.jdbcTemplate.query(
        this.loadQuery(query), params, new BeanPropertyRowMapper<>(clazz));
  }

  public Long save(String query, SqlParameterSource params, String[] keys) {
    log.debug("Saving in the db with with query {} and params {}", query, params);
    KeyHolder keyHolder = new GeneratedKeyHolder();
    this.jdbcTemplate.update(this.loadQuery(query), params, keyHolder, keys);
    return Optional.ofNullable(keyHolder.getKey()).orElse(0).longValue();
  }

  public void save(String query, SqlParameterSource params) {
    log.debug("Saving in the db with with query {} and params {}", query, params);
    this.jdbcTemplate.update(this.loadQuery(query), params);
  }

  private String loadQuery(String res) {
    try {
      DefaultResourceLoader loader = new DefaultResourceLoader();
      Resource resource = loader.getResource(res);
      Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8.name());
      return FileCopyUtils.copyToString(reader);
    } catch (Exception e) {
      String errorMessage = String.format("Error cargando recurso SQL %s", res);
      log.error(errorMessage);
      throw new IOException(errorMessage, e);
    }
  }
}
