package ar.meli.showsbooking.infra.persistence.service;

import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.infra.persistence.model.RoomDto;
import ar.meli.showsbooking.infra.persistence.model.TheaterDto;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class RoomJdbcService {

  private static final String GET_ROOMS_BY_THEATER_PATH = "sql/select-rooms-by-theater.sql";
  private static final String GET_ROOM_BY_ID_PATH = "sql/select-rooms-by-theater.sql";

  private final PersistenceJdbcService persistenceJdbcService;
  private final DaoService daoService;

  @Autowired
  RoomJdbcService(PersistenceJdbcService persistenceJdbcService, DaoService daoService) {
    this.persistenceJdbcService = persistenceJdbcService;
    this.daoService = daoService;
  }

  public List<RoomDto> findByTheater(TheaterDto theaterDto) {
    try {
      log.info("Looking all rooms of the {} theater", theaterDto.getId());
      final var params = new MapSqlParameterSource().addValue("theater_id", theaterDto.getId());
      return daoService.findAll(GET_ROOMS_BY_THEATER_PATH, params, RoomDto.class).stream()
          .map(this::retrievePerformances)
          .collect(Collectors.toList());
    } catch (DataAccessException ex) {
      log.error(
          "Fatal error has occurred while retrieves all rooms for the theater {}",
          theaterDto.getId(),
          ex);
      throw new PersistenceException(ErrorCode.BOOKING.getDescription(), ex);
    }
  }

  private RoomDto retrievePerformances(RoomDto roomDto) {
    final var performancesDto = this.persistenceJdbcService.findByRoom(roomDto);
    roomDto.setPerformances(performancesDto);
    return roomDto;
  }
}
