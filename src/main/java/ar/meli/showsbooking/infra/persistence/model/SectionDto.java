package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Section;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class SectionDto {

  private Long id;
  private BigDecimal price;
  private List<ChairDto> sits;
  private RoomDto room;

  public Section toDomain() {
    return Section.builder()
        .id(this.id.toString())
        .chairs(this.sits.stream().map(ChairDto::toDomain).collect(Collectors.toList()))
        .build();
  }
}
