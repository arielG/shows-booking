package ar.meli.showsbooking.infra.persistence;

import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.app.port.out.TheaterRepositoryPort;
import ar.meli.showsbooking.domain.Theater;
import ar.meli.showsbooking.infra.persistence.model.TheaterDto;
import ar.meli.showsbooking.infra.persistence.service.DaoService;
import ar.meli.showsbooking.infra.persistence.service.RoomJdbcService;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class TheaterRepository implements TheaterRepositoryPort {

  private static final String GET_THEATERS_QUERY = "sql/select-theaters.sql";

  private final RoomJdbcService roomsJdbcService;
  private final DaoService daoService;

  @Autowired
  public TheaterRepository(RoomJdbcService roomsJdbcService, DaoService daoService) {
    this.roomsJdbcService = roomsJdbcService;
    this.daoService = daoService;
  }

  @Override
  public List<Theater> getAll() {
    try {
      log.info("Looking all theaters in the database");
      return daoService.findAll(GET_THEATERS_QUERY, TheaterDto.class).stream()
          .map(this::retrieveRooms)
          .map(TheaterDto::toDomain)
          .collect(Collectors.toList());
    } catch (DataAccessException ex) {
      log.error("Error fatal has occurred while trying get all the theaters", ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }

  private TheaterDto retrieveRooms(TheaterDto theaterDto) {
    final var roomsDto = roomsJdbcService.findByTheater(theaterDto);
    theaterDto.setRooms(roomsDto);
    return theaterDto;
  }
}
