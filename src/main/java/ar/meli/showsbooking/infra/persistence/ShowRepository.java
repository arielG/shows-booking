package ar.meli.showsbooking.infra.persistence;

import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.app.port.out.ShowRepositoryPort;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.infra.persistence.model.ShowDto;
import ar.meli.showsbooking.infra.persistence.service.DaoService;
import ar.meli.showsbooking.infra.persistence.service.PersistenceJdbcService;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class ShowRepository implements ShowRepositoryPort {

  private static final String GET_SHOWS_QUERY = "sql/select-shows.sql";

  private final PersistenceJdbcService persistenceJdbcService;
  private final DaoService daoService;

  @Autowired
  public ShowRepository(PersistenceJdbcService persistenceJdbcService, DaoService daoService) {
    this.persistenceJdbcService = persistenceJdbcService;
    this.daoService = daoService;
  }

  @Override
  public List<Show> getAll() {
    try {
      log.info("Looking all shows in the database");
      final var shows =
          daoService.findAll(GET_SHOWS_QUERY, ShowDto.class).stream()
              .map(this::retrievePerformances)
              .map(ShowDto::toDomain)
              .collect(Collectors.toList());
      log.info("Shows found: {}", shows);
      return shows;
    } catch (DataAccessException ex) {
      log.error("Error fatal has occurred while trying get all the theaters", ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }

  private ShowDto retrievePerformances(ShowDto showDto) {
    final var performances = persistenceJdbcService.findByShow(showDto);
    showDto.setPerformances(performances);
    return showDto;
  }
}
