package ar.meli.showsbooking.infra.persistence;

import ar.meli.showsbooking.app.exception.BusyChairException;
import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.app.port.out.BookingRepositoryPort;
import ar.meli.showsbooking.domain.Booking;
import ar.meli.showsbooking.infra.persistence.model.BookingDto;
import ar.meli.showsbooking.infra.persistence.model.ChairDto;
import ar.meli.showsbooking.infra.persistence.service.DaoService;
import ar.meli.showsbooking.infra.persistence.service.PersistenceJdbcService;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class BookingRepository implements BookingRepositoryPort {

  private static final String INSERT_BOOKING_PATH = "sql/insert-booking.sql";
  private static final String INSERT_PERFORMANCE_CHAIRS = "sql/insert-performance-chairs.sql";
  private static final String GET_CHAIR_BY_ID_PATH = "sql/select-chair-by-id.sql";

  private final PersistenceJdbcService persistenceJdbcService;
  private final DaoService daoService;

  @Autowired
  public BookingRepository(PersistenceJdbcService persistenceJdbcService, DaoService daoService) {
    this.persistenceJdbcService = persistenceJdbcService;
    this.daoService = daoService;
  }

  @Override
  @Transactional
  public Booking toBook(Booking booking) {
    try {
      log.info(
          "Client {} is trying to book the sits {} to se the performance {}",
          booking.getClient().getName(),
          booking.getChairs(),
          booking.getPerformance());

      final var bookingDto = BookingDto.of(booking);

      final var chairsReserved = this.reserveChairs(bookingDto);
      log.info("Chairs reserved for the performance {}", bookingDto.getPerformance());

      final var performanceDto =
          this.persistenceJdbcService.findById(booking.getPerformance().getId());

      bookingDto.setPerformance(performanceDto);
      bookingDto.setChairs(
          chairsReserved.stream()
              .map(chairDto -> chairDto.withPrice(performanceDto.getPrice()))
              .collect(Collectors.toList()));

      final var bookingId =
          this.daoService.save(INSERT_BOOKING_PATH, bookingDto.toParams(), new String[] {"id"});
      log.info("Booking created {}", bookingId);

      bookingDto.setId(bookingId);
      return bookingDto.toDomain();

    } catch (DataAccessException ex) {
      log.error("Error fatal has occurred while trying to create the booking {}", booking);
      throw new PersistenceException(ErrorCode.BOOKING.getDescription(), ex);
    }
  }

  private List<ChairDto> reserveChairs(BookingDto bookingDto) {
    return bookingDto.getChairs().stream()
        .map(chairDto -> saveChair(bookingDto, chairDto))
        .collect(Collectors.toList());
  }

  private ChairDto saveChair(BookingDto bookingDto, ChairDto chairDto) {
    try {
      this.daoService.save(
          INSERT_PERFORMANCE_CHAIRS, chairDto.toSqlParams(bookingDto.getPerformance().getId()));
      final var params = new MapSqlParameterSource().addValue("id", chairDto.getId());
      return this.daoService
          .findBy(GET_CHAIR_BY_ID_PATH, params, ChairDto.class)
          .orElseThrow(
              () -> {
                log.error("Error retrieving chair with id {}", chairDto.getId());
                throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription());
              });
    } catch (DuplicateKeyException ex) {
      log.info("The chair {} is busy", chairDto.getId());
      throw new BusyChairException(
          String.format(ErrorCode.CHAIR_RESERVATION.getDescription(), chairDto.getId()), ex);
    } catch (DataAccessException ex) {
      log.error("DB fatal error", ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }
}
