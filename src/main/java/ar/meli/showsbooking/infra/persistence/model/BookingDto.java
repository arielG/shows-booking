package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Booking;
import lombok.Data;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class BookingDto {
  private Long id;
  private ClientDto client;
  private PerformanceDto performance;
  private List<ChairDto> chairs;

  public static BookingDto of(Booking booking) {
    final var bookingDto = new BookingDto();
    bookingDto.setClient(ClientDto.of(booking.getClient()));
    bookingDto.setPerformance(PerformanceDto.of(booking.getPerformance()));
    bookingDto.setChairs(
        booking.getChairs().stream().map(ChairDto::of).collect(Collectors.toList()));
    return bookingDto;
  }

  public Booking toDomain() {
    return Booking.builder()
        .id(this.id)
        .client(this.client.toDomain())
        .performance(this.performance.toDomain())
        .chairs(
            Optional.ofNullable(this.chairs)
                .map(
                    chairDtos ->
                        chairDtos.stream().map(ChairDto::toDomain).collect(Collectors.toList()))
                .orElse(List.of()))
        .build();
  }

  public SqlParameterSource toParams() {
    return new MapSqlParameterSource()
        .addValue("dni", this.getClient().getDni())
        .addValue("performance_id", this.getPerformance().getId());
  }
}
