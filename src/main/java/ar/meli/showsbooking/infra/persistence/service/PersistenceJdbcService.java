package ar.meli.showsbooking.infra.persistence.service;

import ar.meli.showsbooking.app.exception.PersistenceException;
import ar.meli.showsbooking.infra.persistence.model.PerformanceDto;
import ar.meli.showsbooking.infra.persistence.model.RoomDto;
import ar.meli.showsbooking.infra.persistence.model.ShowDto;
import ar.meli.showsbooking.shared.ErrorCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class PersistenceJdbcService {

  private static final String GET_PERFORMANCE_BY_ID_PATH = "sql/select-performance-by-id.sql";
  private static final String GET_PERFORMANCES_BY_ROOM_PATH = "sql/select-performances-by-room.sql";
  private static final String GET_PERFORMANCES_BY_SHOW_PATH = "sql/select-performances-by-show.sql";

  private final ChairsJdbcService chairsJdbcService;
  private final DaoService daoService;

  @Autowired
  PersistenceJdbcService(ChairsJdbcService chairsJdbcService, DaoService daoService) {
    this.chairsJdbcService = chairsJdbcService;
    this.daoService = daoService;
  }

  public PerformanceDto findById(Long id) {
    try {
      log.info("Looking performance with id {}", id);
      final var params = new MapSqlParameterSource().addValue("id", id);
      final var performances =
          this.daoService
              .findBy(GET_PERFORMANCE_BY_ID_PATH, params, PerformanceDto.class)
              .map(performance -> performance.withRoom(new RoomDto().withId(id)))
              .map(this::retrieveAvailableChairs);
      log.info("Looking performance with id {}", id);
      return performances.orElseThrow(
          () -> {
            log.info("Performance {} not found in the db", id);
            throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription());
          });
    } catch (DataAccessException ex) {
      log.error("Fatal error has occurred while retrieves performance with id {}", id, ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }

  public List<PerformanceDto> findByRoom(RoomDto roomDto) {
    try {
      log.info("Looking all performances of the {} room", roomDto.getId());
      final var params = new MapSqlParameterSource().addValue("room_id", roomDto.getId());
      final var performances =
          this.daoService
              .findAll(GET_PERFORMANCES_BY_ROOM_PATH, params, PerformanceDto.class)
              .stream()
              .map(performance -> performance.withRoom(roomDto))
              .map(this::retrieveAvailableChairs)
              .collect(Collectors.toList());
      log.info("Looking all performances of the {} room", roomDto.getId());
      return performances;
    } catch (DataAccessException ex) {
      log.error(
          "Fatal error has occurred while retrieves all performances for the room {}",
          roomDto.getId(),
          ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }

  public List<PerformanceDto> findByShow(ShowDto showDto) {
    try {
      log.info("Looking all performances of the {} show", showDto.getId());
      final var params = new MapSqlParameterSource().addValue("show_id", showDto.getId());
      final var performances =
          this.daoService
              .findAll(GET_PERFORMANCES_BY_SHOW_PATH, params, PerformanceDto.class)
              .stream()
              .map(performance -> performance.withShow(showDto))
              .map(
                  performance ->
                      performance.withRoom(new RoomDto().withId(performance.getRoomId())))
              .map(this::retrieveAvailableChairs)
              .collect(Collectors.toList());
      log.info("Looking all performances of the {} show", showDto.getId());
      return performances;
    } catch (DataAccessException ex) {
      log.error(
          "Fatal error has occurred while retrieves all performances for the show {}",
          showDto.getId(),
          ex);
      throw new PersistenceException(ErrorCode.PERSISTENCE.getDescription(), ex);
    }
  }

  private PerformanceDto retrieveAvailableChairs(PerformanceDto performanceDto) {
    final var availableChairs =
        chairsJdbcService.findAvailable(performanceDto).stream()
            .map(chair -> chair.withPrice(performanceDto.getPrice()))
            .collect(Collectors.toList());
    performanceDto.setAvailable(availableChairs);
    return performanceDto;
  }
}
