package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Chair;
import lombok.Data;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Data
public class ChairDto {

  private Long id;
  private SectionDto section;
  private BigDecimal price;
  private List<BookingDto> bookings;
  private List<PerformanceDto> performances;

  public static ChairDto of(Chair chair) {
    final var sitDto = new ChairDto();
    sitDto.setId(chair.getId());
    return sitDto;
  }

  public Chair toDomain() {
    return Chair.builder().id(this.id).price(this.price).build();
  }

  public SqlParameterSource toSqlParams(Long otherId) {
    return new MapSqlParameterSource()
        .addValue("chair_id", this.id)
        .addValue("performance_id", otherId);
  }

  public ChairDto withPrice(BigDecimal price) {
    return Optional.ofNullable(this.price)
        .map(sectionPrice -> this)
        .orElseGet(
            () -> {
              this.price = price;
              return this;
            });
  }
}
