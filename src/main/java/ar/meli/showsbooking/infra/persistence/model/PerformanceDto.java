package ar.meli.showsbooking.infra.persistence.model;

import ar.meli.showsbooking.domain.Performance;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class PerformanceDto {

  private Long id;
  @With private ShowDto show;
  private LocalDateTime date;
  private BigDecimal price;
  private Long roomId;
  @With private RoomDto room;
  @With private TheaterDto theaterDto;
  private List<BookingDto> bookings;
  private List<ChairDto> available;

  public static PerformanceDto of(Performance performance) {
    final var performanceDto = new PerformanceDto();
    performanceDto.setId(performance.getId());
    return performanceDto;
  }

  public Performance toDomain() {
    return Performance.builder()
        .id(id)
        .date(this.date)
        .price(this.price)
        .available(
            Optional.ofNullable(this.available)
                .map(
                    available ->
                        available.stream().map(ChairDto::toDomain).collect(Collectors.toList()))
                .orElse(List.of()))
        .build();
  }
}
