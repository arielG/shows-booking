package ar.meli.showsbooking.infra.controller;

import ar.meli.showsbooking.app.port.in.ToBookCommand;
import ar.meli.showsbooking.domain.Booking;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("/api/v1/bookings")
public class BookingController {

  private final ToBookCommand toBookCommand;

  @Autowired
  public BookingController(ToBookCommand toBookCommand) {
    this.toBookCommand = toBookCommand;
  }

  @PostMapping
  @Secured("CLIENT")
  @ResponseStatus(HttpStatus.CREATED)
  Booking toBook(@RequestBody Booking booking, HttpServletRequest request) {
    log.info("Post -> Endpoint {} was hit", request.getRequestURI());
    return toBookCommand.execute(booking);
  }
}
