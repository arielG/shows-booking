package ar.meli.showsbooking.infra.controller;

import ar.meli.showsbooking.app.port.in.GetTheatersQuery;
import ar.meli.showsbooking.domain.Theater;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1/theaters")
public class TheaterController {

  private final GetTheatersQuery getTheatersQuery;

  @Autowired
  public TheaterController(GetTheatersQuery getTheatersQuery) {
    this.getTheatersQuery = getTheatersQuery;
  }

  @GetMapping
  @Secured("CLIENT")
  List<Theater> getAll(HttpServletRequest request) {
    log.info("Get -> Endpoint {} was hit", request.getRequestURI());
    return getTheatersQuery.execute();
  }
}
