package ar.meli.showsbooking.infra.controller;

import ar.meli.showsbooking.app.port.in.GetShowsQuery;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.domain.filter.ShowFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/v1/shows")
public class ShowController {

  private final GetShowsQuery getShowsQuery;

  @Autowired
  public ShowController(GetShowsQuery getShowsQuery) {
    this.getShowsQuery = getShowsQuery;
  }

  @GetMapping
  @Secured("CLIENT")
  ResponseEntity<List<Show>> getAll(
      HttpServletRequest request,
      @RequestParam(required = false) BigDecimal fromPrice,
      @RequestParam(required = false) BigDecimal toPrice,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          LocalDateTime fromDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          LocalDateTime toDate,
      @RequestParam(required = false) Boolean sortById,
      @RequestParam(required = false) Boolean sortByName,
      @RequestParam(required = false) ShowFilter.Direction direction) {
    log.info("Get -> Endpoint {} was hit", request.getRequestURI());
    return ResponseEntity.ok(
        getShowsQuery.execute(
            ShowFilter.of(
                fromDate,
                toDate,
                fromPrice,
                toPrice,
                ShowFilter.Sort.builder()
                    .sortById(sortById)
                    .sortByName(sortByName)
                    .direction(Optional.ofNullable(direction).orElse(ShowFilter.Direction.ASC))
                    .build())));
  }

  @GetMapping("/{id}")
  @Secured("CLIENT")
  ResponseEntity<Show> getById(HttpServletRequest request, @PathVariable Long id) {
    log.info("Get -> Endpoint {} was hit", request.getRequestURI());
    return getShowsQuery.execute(ShowFilter.of(id)).stream()
        .findAny()
        .map(ResponseEntity::ok)
        .orElseGet(() -> ResponseEntity.badRequest().build());
  }
}
