package ar.meli.showsbooking.shared;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

@Configuration
@EnableWebSecurity
@Order(SecurityProperties.BASIC_AUTH_ORDER - 10)
public class BasicAuthConfig extends WebSecurityConfigurerAdapter {

  private final AuthenticationEntryPoint authenticationEntryPoint;

  @Autowired
  public BasicAuthConfig(AuthenticationEntryPoint authenticationEntryPoint) {
    this.authenticationEntryPoint = authenticationEntryPoint;
  }

  @Autowired
  public void configureGlobal(
      AuthenticationManagerBuilder authBuilder, PasswordEncoder passwordEncoder) throws Exception {
    authBuilder
        .inMemoryAuthentication()
        .withUser("randomClient")
        .password(passwordEncoder.encode("clientPass"))
        .roles("CLIENT");
    authBuilder
        .inMemoryAuthentication()
        .withUser("admin")
        .password(passwordEncoder.encode("adminPass"))
        .roles("ADMIN");
  }

  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    httpSecurity
        .authorizeRequests()
        .antMatchers("/api/v1/theaters", "/api/v1/shows", "/api/v1/bookings")
        .fullyAuthenticated()
        .and()
        .authorizeRequests()
        .antMatchers("/h2-console/**")
        .permitAll()
        .and()
        .httpBasic()
        .authenticationEntryPoint(authenticationEntryPoint);

    httpSecurity.csrf().disable();
    httpSecurity.headers().frameOptions().disable();

    httpSecurity.addFilterAfter(new CustomFilter(), BasicAuthenticationFilter.class);
  }

  static class CustomFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
      chain.doFilter(request, response);
    }
  }
}
