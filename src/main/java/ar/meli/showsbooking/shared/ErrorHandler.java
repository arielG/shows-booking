package ar.meli.showsbooking.shared;

import ar.meli.showsbooking.app.exception.ApplicationException;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Slf4j
@ControllerAdvice
public class ErrorHandler {

  private final HttpServletRequest request;

  @Autowired
  public ErrorHandler(HttpServletRequest request) {
    this.request = request;
  }

  @ExceptionHandler(ApplicationException.class)
  public ResponseEntity<ApiErrorResponse> handle(ApplicationException ex) {
    log.error(HttpStatus.BAD_REQUEST.getReasonPhrase(), ex);
    return buildErrorResponse(HttpStatus.BAD_REQUEST, ex);
  }

  @ExceptionHandler({
    HttpMessageNotReadableException.class,
    MethodArgumentTypeMismatchException.class
  })
  public ResponseEntity<ApiErrorResponse> handle(NestedRuntimeException ex) {
    log.error(HttpStatus.BAD_REQUEST.getReasonPhrase(), ex);
    return buildErrorResponse(HttpStatus.BAD_REQUEST, ErrorCode.BAD_REQUEST);
  }

  @ExceptionHandler(Throwable.class)
  public ResponseEntity<ApiErrorResponse> handle(Throwable throwable) {
    log.error(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), throwable);
    return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ErrorCode.GENERIC);
  }

  private ResponseEntity<ApiErrorResponse> buildErrorResponse(
      HttpStatus httpStatus, ApplicationException appError) {
    return this.buildErrorResponse(httpStatus, appError.getErrorCode(), appError.getMessage());
  }

  private ResponseEntity<ApiErrorResponse> buildErrorResponse(
      HttpStatus httpStatus, ErrorCode errorCode) {
    return this.buildErrorResponse(httpStatus, errorCode, errorCode.getDescription());
  }

  private ResponseEntity<ApiErrorResponse> buildErrorResponse(
      HttpStatus httpStatus, ErrorCode errorCode, String message) {
    return new ResponseEntity<>(
        ApiErrorResponse.builder()
            .code(errorCode.getCode())
            .status(httpStatus.getReasonPhrase())
            .message(message)
            .path(request.getRequestURI())
            .timestamp(LocalDateTime.now())
            .build(),
        httpStatus);
  }

  @Value
  @Builder
  @NonNull
  @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
  private static class ApiErrorResponse {
    private static final String DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss[.SSSSSSSSS]['Z']";
    String code;
    String status;
    String message;
    String path;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_PATTERN)
    LocalDateTime timestamp;
  }
}
