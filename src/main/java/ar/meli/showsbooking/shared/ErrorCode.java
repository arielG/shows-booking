package ar.meli.showsbooking.shared;

public enum ErrorCode {
  GENERIC("generic_error", "Unexpected error, take a beer and chill out"),
  CHAIR_RESERVATION(
      "chair_reservation_error", "The chair %s is busy, sorry, please choose another one"),
  BOOKING("booking_error", "Booking error occurred"),
  SEARCH("search_error", ""),
  PERSISTENCE("persistence_error", "An error was occurred while trying to get data from the db"),
  BAD_REQUEST(
      "bad_request_error",
      "What are you trying to do? Please check the input data and don't waste my time"),
  CACHE_ERROR("cache_error", "Oh no, the cache doesn't work, this is going to take a while"),
  SHOWS("shows_error", "Error occurred trying to get the shows, please, be patient");

  private final String code;
  private final String description;

  ErrorCode(String code, String description) {
    this.code = code;
    this.description = description;
  }

  public String getCode() {
    return this.code;
  }

  public String getDescription() {
    return description;
  }
}
