package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Value
@Builder
public class Performance {
  Long id;
  Show show;
  LocalDateTime date;
  BigDecimal price;
  List<Chair> available;
}
