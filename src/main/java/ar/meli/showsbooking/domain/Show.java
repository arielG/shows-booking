package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Show {
  private Long id;
  private String name;
  private List<Performance> performances;
}
