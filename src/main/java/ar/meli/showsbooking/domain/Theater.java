package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Theater {
  Long id;
  String name;
  List<Room> rooms;
}
