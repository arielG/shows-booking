package ar.meli.showsbooking.domain.filter;

import ar.meli.showsbooking.domain.Show;
import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Value
@Builder
public class ShowFilter {
  Long id;
  LocalDateTime fromDate;
  LocalDateTime toDate;
  BigDecimal fromPrice;
  BigDecimal toPrice;
  Sort sort;

  public static ShowFilter empty() {
    return ShowFilter.builder().sort(Sort.builder().direction(Direction.ASC).build()).build();
  }

  public static ShowFilter of(Long showId) {
    return ShowFilter.builder().id(showId).build();
  }

  public static ShowFilter of(LocalDateTime from, LocalDateTime to) {
    return ShowFilter.builder()
        .fromDate(from)
        .toDate(to)
        .sort(Sort.builder().direction(Direction.ASC).build())
        .build();
  }

  public static ShowFilter of(BigDecimal from, BigDecimal to) {
    return ShowFilter.builder().fromPrice(from).toPrice(to).build();
  }

  public static ShowFilter of(
      LocalDateTime fromDate, LocalDateTime toDate, BigDecimal fromPrice, BigDecimal toPrice) {
    return ShowFilter.builder()
        .fromDate(fromDate)
        .toDate(toDate)
        .fromPrice(fromPrice)
        .toPrice(toPrice)
        .build();
  }

  public static ShowFilter of(
      LocalDateTime fromDate,
      LocalDateTime toDate,
      BigDecimal fromPrice,
      BigDecimal toPrice,
      Sort sort) {
    return ShowFilter.builder()
        .fromDate(fromDate)
        .toDate(toDate)
        .fromPrice(fromPrice)
        .toPrice(toPrice)
        .sort(sort)
        .build();
  }

  public boolean applyIdFilter(Show show) {
    return Objects.isNull(this.id) || Objects.equals(this.id, show.getId());
  }

  public boolean applyDateFilter(Show show) {
    final var performancesMatch =
        show.getPerformances().stream()
            .filter(
                performance ->
                    (Objects.isNull(this.fromDate) || performance.getDate().isAfter(this.fromDate))
                        && (Objects.isNull(this.toDate)
                            || performance.getDate().isBefore(this.toDate)))
            .collect(Collectors.toList());

    return Optional.of(performancesMatch.isEmpty())
        .filter(isEmpty -> isEmpty)
        .map(notPerformanceMatch -> false)
        .orElseGet(
            () -> {
              show.setPerformances(performancesMatch);
              return true;
            });
  }

  public boolean applyPriceFilter(Show show) {
    final var performancesMatch =
        show.getPerformances().stream()
            .filter(
                performance ->
                    (Objects.isNull(this.fromPrice)
                            || performance.getPrice().compareTo(this.fromPrice) >= 0)
                        && (Objects.isNull(this.toPrice)
                            || performance.getPrice().compareTo(this.toPrice) <= 0))
            .collect(Collectors.toList());

    return Optional.of(performancesMatch.isEmpty())
        .filter(isEmpty -> isEmpty)
        .map(notPerformanceMatch -> false)
        .orElseGet(
            () -> {
              show.setPerformances(performancesMatch);
              return true;
            });
  }

  public List<Show> sortShows(List<Show> shows) {
    return Optional.ofNullable(sort.getSortById())
        .filter(sortById -> sortById)
        .map(sortById -> sortById(shows))
        .orElseGet(
            () ->
                Optional.ofNullable(sort.getSortByName())
                    .filter(sortByName -> sortByName)
                    .map(sortById -> sortByName(shows))
                    .orElse(shows));
  }

  private List<Show> sortByName(List<Show> shows) {
    shows.sort(
        Comparator.comparing(
            Show::getName,
            Optional.ofNullable(this.sort.getDirection()).orElse(Direction.ASC).getComparator()));
    return shows;
  }

  private List<Show> sortById(List<Show> shows) {
    shows.sort(Comparator.comparing(Show::getId, sort.getDirection().getComparator()));
    return shows;
  }

  public enum Direction {
    ASC,
    DESC;

    public <T extends Comparable<? super T>> Comparator<T> getComparator() {
      return ASC.equals(this) ? Comparator.naturalOrder() : Comparator.reverseOrder();
    }
  }

  @Value
  @Builder
  public static class Sort {
    Boolean sortById;
    Boolean sortByName;
    Direction direction;
  }
}
