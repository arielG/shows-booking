package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;
import lombok.With;

import java.util.List;

@Value
@Builder
public class Booking {
  @With Long id;
  Client client;
  Performance performance;
  List<Chair> chairs;
}
