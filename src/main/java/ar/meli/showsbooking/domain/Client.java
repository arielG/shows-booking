package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Client {
  String dni;
  String name;
}
