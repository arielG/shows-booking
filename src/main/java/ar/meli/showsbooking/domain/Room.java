package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Room {
  Long id;
  List<Section> sections;
  List<Performance> performances;
}
