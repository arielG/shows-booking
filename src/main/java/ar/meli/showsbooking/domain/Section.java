package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.List;

@Value
@Builder
public class Section {
  String id;
  BigDecimal price;
  List<Chair> chairs;
}
