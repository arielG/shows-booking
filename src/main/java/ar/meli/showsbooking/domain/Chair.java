package ar.meli.showsbooking.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;

@Value
@Builder
public class Chair {
  Long id;
  BigDecimal price;
}
