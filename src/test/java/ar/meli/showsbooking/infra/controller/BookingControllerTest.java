package ar.meli.showsbooking.infra.controller;

import ar.meli.showsbooking.app.port.in.ToBookCommand;
import ar.meli.showsbooking.utils.StubsFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class BookingControllerTest {

  private static final String resource = "/api/v1/bookings";
  @MockBean ToBookCommand toBookCommand;
  @Autowired private ObjectMapper objectMapper;
  @Autowired private MockMvc mockMvc;

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/bookings is called,"
          + " and all the required information is provided"
          + " then the new booking should be returned and the corresponding 201 status code")
  void whenAllTheBookingInformationIsProvidedThenTheNewBookingIsReturned() throws Exception {

    final var request = StubsFactory.loadJson("contract/new-booking-request.json");
    final var newBooking = StubsFactory.newBooking();

    when(toBookCommand.execute(newBooking)).thenReturn(newBooking);

    mockMvc
        .perform(post(resource).contentType(MediaType.APPLICATION_JSON).content(request))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(content().json(objectMapper.writeValueAsString(newBooking)));

    verify(toBookCommand, only()).execute(newBooking);
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/bookings is called, "
          + "and there isn't body provided"
          + "then should respond with bad request 400 status code")
  void whenThereAreNotPerformancesShouldBeReturned() throws Exception {

    mockMvc.perform(post(resource)).andDo(print()).andExpect(status().isBadRequest());

    verify(toBookCommand, never()).execute(any());
  }

  @Test
  @DisplayName(
      "When the endpoint /api/v1/bookings is called, "
          + "and there is a unauthorized client"
          + "then should return an unauthorized code")
  void whenAnUnauthorizedUserShouldRespondWithAnUnauthorizedResponse() throws Exception {

    mockMvc
        .perform(get(resource))
        .andDo(print())
        .andExpect(status().isUnauthorized())
        .andExpect(
            content()
                .string(
                    "HTTP Status 401 - Full authentication is required to access this resource\n"));
  }
}
