package ar.meli.showsbooking.infra.controller;

import ar.meli.showsbooking.app.port.in.GetShowsQuery;
import ar.meli.showsbooking.app.port.in.GetTheatersQuery;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.domain.filter.ShowFilter;
import ar.meli.showsbooking.utils.StubsFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class ShowControllerTest {

  private static final String SHOWS_PATH = "/api/v1/shows";
  private static final String SHOWS_BY_ID_PATH = "/api/v1/shows/{id}";

  @MockBean GetTheatersQuery getTheatersQuery;
  @MockBean GetShowsQuery getShowsQuery;
  @Autowired private ObjectMapper objectMapper;
  @Autowired private MockMvc mockMvc;

  @Test
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and none is authenticated"
          + "then should return an unauthorized code")
  void whenAnUnauthorizedUserShouldRespondWithAnUnauthorizedResponse() throws Exception {

    mockMvc
        .perform(get(SHOWS_PATH))
        .andDo(print())
        .andExpect(status().isUnauthorized())
        .andExpect(
            content()
                .string(
                    "HTTP Status 401 - Full authentication is required to access this resource\n"));

    verify(getShowsQuery, never()).execute(ShowFilter.empty());
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and there aren't performances to show"
          + "then should return an empty list")
  void whenThereAreNotPerformancesShouldBeReturned() throws Exception {

    final List<Show> emptyPerformanceList = List.of();

    when(getShowsQuery.execute(ShowFilter.empty())).thenReturn(emptyPerformanceList);

    mockMvc
        .perform(get(SHOWS_PATH))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(emptyPerformanceList)));

    verify(getShowsQuery, only()).execute(ShowFilter.empty());
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and no filter is applied"
          + "then all shows should be returned")
  void whenThereArePerformancesShouldBeReturned() throws Exception {

    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    final var filter = ShowFilter.empty();

    when(getShowsQuery.execute(filter)).thenReturn(shows);

    mockMvc
        .perform(get(SHOWS_PATH))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(shows)));

    verify(getShowsQuery, only()).execute(filter);
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and the client filtered by id"
          + "but non show match"
          + "then a bad request should be responded")
  void whenTheClientFilterByShowIdButNonMatchABadRequestShouldBeReturned() throws Exception {

    final var idFilter = 1L;

    final List<Show> shows = List.of();

    final var filter = ShowFilter.of(idFilter);

    when(getShowsQuery.execute(ShowFilter.of(idFilter))).thenReturn(shows);

    mockMvc
        .perform(get(SHOWS_BY_ID_PATH, idFilter))
        .andDo(print())
        .andExpect(status().isBadRequest());

    verify(getShowsQuery, only()).execute(filter);
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and the client filtered by id"
          + "then only that show should be returned")
  void whenTheClientFilterByShowIdThenOnlyThatShowShouldBeReturned() throws Exception {

    final var idFilter = 1L;

    final var filter = ShowFilter.of(idFilter);

    final var shows = List.of(StubsFactory.getFirstShowPerformances());

    final var showFound = StubsFactory.getFirstShowPerformances();

    when(getShowsQuery.execute(filter)).thenReturn(shows);

    mockMvc
        .perform(get(SHOWS_BY_ID_PATH, idFilter))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(showFound)));

    verify(getShowsQuery, only()).execute(filter);
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and the client filtered by dates [2022-01-01 00:00, 2022-03-01 00:00]"
          + "then only the show with performances between should be returned")
  void whenTheClientFilterByShowDatesThenOnlyThatShowsShouldBeReturned() throws Exception {

    final var filter =
        ShowFilter.of(
            LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0),
            LocalDateTime.of(2022, Month.MARCH, 1, 0, 0));

    final var showsFound =
        List.of(StubsFactory.getFirstShowPerformances(StubsFactory.getFirstPerformance()));

    when(getShowsQuery.execute(filter)).thenReturn(showsFound);

    mockMvc
        .perform(
            get(SHOWS_PATH)
                .param("fromDate", "2022-01-01T00:00")
                .param("toDate", "2022-03-01T00:00"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(showsFound)));

    verify(getShowsQuery, only()).execute(filter);
  }

  @Test
  @WithMockUser(value = "randomClient")
  @DisplayName(
      "When the endpoint /api/v1/shows is called, "
          + "and a client is authenticated"
          + "and the client filtered by price [50, 200]"
          + "then only the show with performances between should be returned")
  void whenTheClientFilterByShowPricesThenOnlyThatShowsShouldBeReturned() throws Exception {

    final var filter = ShowFilter.of(BigDecimal.valueOf(50.00), BigDecimal.valueOf(200.00));

    final var showsFound =
        List.of(StubsFactory.getFirstShowPerformances(StubsFactory.getFirstPerformance()));

    when(getShowsQuery.execute(any())).thenReturn(showsFound);

    mockMvc
        .perform(get(SHOWS_PATH).param("fromPrice", "50").param("toPrice", "100"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(showsFound)));

    verify(getShowsQuery, only()).execute(any());
  }
}
