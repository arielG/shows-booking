package ar.meli.showsbooking.arch;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;

@AnalyzeClasses(
    packages = "ar.meli.showsbooking",
    importOptions = ImportOption.DoNotIncludeTests.class)
public class LayeredArchitectureRulesTest {

  private static final String DOMAIN = "domain";
  private static final String APP = "application";
  private static final String INFRA = "infrastructure";
  private static final String SHARED = "shared";

  @ArchTest
  static final ArchRule layeDependenciesConstraints =
      Architectures.layeredArchitecture()
          .layer(SHARED)
          .definedBy("ar.meli.showsbooking.shared..")
          .layer(INFRA)
          .definedBy("ar.meli.showsbooking.infra..")
          .layer(APP)
          .definedBy("ar.meli.showsbooking.app..")
          .layer(DOMAIN)
          .definedBy("ar.meli.showsbooking.domain..")
          .whereLayer(DOMAIN)
          .mayOnlyBeAccessedByLayers(APP, INFRA, SHARED)
          .whereLayer(APP)
          .mayOnlyBeAccessedByLayers(INFRA, SHARED)
          .whereLayer(INFRA)
          .mayOnlyBeAccessedByLayers(SHARED);
}
