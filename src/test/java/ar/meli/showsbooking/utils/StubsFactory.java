package ar.meli.showsbooking.utils;

import ar.meli.showsbooking.domain.Booking;
import ar.meli.showsbooking.domain.Chair;
import ar.meli.showsbooking.domain.Client;
import ar.meli.showsbooking.domain.Performance;
import ar.meli.showsbooking.domain.Room;
import ar.meli.showsbooking.domain.Show;
import ar.meli.showsbooking.domain.Theater;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StubsFactory {

  public static Show.ShowBuilder getFirstShow() {
    return Show.builder().id(1L).name("The amazing normal man");
  }

  public static Show.ShowBuilder getSecondShow() {
    return Show.builder().id(2L).name("The amazing normal woman");
  }

  public static Show getFirstShowPerformances() {
    return getFirstShow()
        .performances(List.of(getFirstPerformance(), getThirdPerformance()))
        .build();
  }

  public static Show getSecondShowPerformances() {
    return getSecondShow()
        .performances(List.of(getSecondPerformance(), getFourthPerformance()))
        .build();
  }

  public static Show getFirstShowPerformances(Performance... performances) {
    return getFirstShow()
        .performances(Arrays.stream(performances).sequential().collect(Collectors.toList()))
        .build();
  }

  public static Show getSecondShowPerformances(Performance... performances) {
    return getSecondShow()
        .performances(Arrays.stream(performances).sequential().collect(Collectors.toList()))
        .build();
  }

  public static Performance getFirstPerformance() {
    return Performance.builder()
        .id(1L)
        .show(getFirstShow().build())
        .date(LocalDateTime.of(2022, Month.FEBRUARY, 28, 20, 0))
        .price(BigDecimal.valueOf(450))
        .build();
  }

  public static Performance getSecondPerformance() {
    return Performance.builder()
        .id(2L)
        .show(getSecondShow().build())
        .date(LocalDateTime.of(2022, Month.MARCH, 14, 22, 0))
        .price(BigDecimal.valueOf(1000))
        .build();
  }

  public static Performance getThirdPerformance() {
    return Performance.builder()
        .id(3L)
        .show(getFirstShow().build())
        .date(LocalDateTime.of(2022, Month.MARCH, 20, 21, 0))
        .price(BigDecimal.valueOf(70))
        .build();
  }

  public static Performance getFourthPerformance() {
    return Performance.builder()
        .id(4L)
        .show(getSecondShow().build())
        .date(LocalDateTime.of(2022, Month.APRIL, 10, 20, 0))
        .price(BigDecimal.valueOf(45))
        .build();
  }

  public static Booking newBooking() {
    return Booking.builder()
        .client(Client.builder().dni("95799766").name("Ariel Gámez").build())
        .performance(Performance.builder().id(1L).build())
        .chairs(
            List.of(
                Chair.builder().id(1L).build(),
                Chair.builder().id(2L).build(),
                Chair.builder().id(3L).build(),
                Chair.builder().id(4L).build()))
        .build();
  }

  public static String loadJson(String path) throws IOException {
    final DefaultResourceLoader resourceLoader = new DefaultResourceLoader();
    final Resource resource = resourceLoader.getResource(path);
    final InputStreamReader reader =
        new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8.name());
    return FileCopyUtils.copyToString(reader);
  }

  public static Theater getFirstTheater() {
    return Theater.builder()
        .id(1L)
        .name("El Gran Teatro de La Habana")
        .rooms(
            List.of(
                Room.builder().id(1L).performances(List.of(getFirstPerformance())).build(),
                Room.builder().id(2L).performances(List.of(getSecondPerformance())).build()))
        .build();
  }

  public static Theater getSecondTheater() {
    return Theater.builder()
        .id(2L)
        .name("Colón")
        .rooms(
            List.of(
                Room.builder()
                    .id(1L)
                    .performances(List.of(StubsFactory.getThirdPerformance()))
                    .build(),
                Room.builder()
                    .id(2L)
                    .performances(List.of(StubsFactory.getFourthPerformance()))
                    .build()))
        .build();
  }
}
