package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.port.out.ShowCachePort;
import ar.meli.showsbooking.app.port.out.ShowRepositoryPort;
import ar.meli.showsbooking.domain.filter.ShowFilter;
import ar.meli.showsbooking.utils.StubsFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("Acceptance criteria for the use case that get theaters")
class GetShowsUseCaseTest {

  @Mock ShowRepositoryPort showRepositoryPort;
  @Mock ShowCachePort showCachePort;
  @InjectMocks GetShowsUseCase getShowsUseCase;

  @Test
  @DisplayName(
      "When the use case is executed"
          + "without any filter"
          + "then all shows with his performances should be returned")
  void whenAllShowsAreRequestedThenShouldBeReturned() {

    final var filterHash = ShowFilter.empty().hashCode();
    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    when(showCachePort.get(filterHash)).thenReturn(Optional.empty());
    when(showCachePort.save(filterHash, shows)).thenReturn(shows);
    when(showRepositoryPort.getAll()).thenReturn(shows);

    final var actualResult = getShowsUseCase.execute(ShowFilter.empty());

    assertEquals(shows, actualResult);

    verify(showCachePort, times(1)).get(filterHash);
    verify(showCachePort, times(1)).save(filterHash, shows);
    verify(showRepositoryPort, only()).getAll();
  }

  @Test
  @DisplayName(
      "When the use case is executed"
          + "with filtering by date (between [2022-02-01, 2022-03-01])"
          + "then all performances between that rage should be returned")
  void whenAllShowsAreRequestedFilteringByShowThenOnlyThatShowShouldBeReturned() {

    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    final var showFound = List.of(StubsFactory.getFirstShowPerformances());

    final var filter = ShowFilter.of(1L);

    when(showCachePort.get(filter.hashCode())).thenReturn(Optional.empty());
    when(showCachePort.save(filter.hashCode(), showFound)).thenReturn(showFound);
    when(showRepositoryPort.getAll()).thenReturn(shows);

    final var actualResult = getShowsUseCase.execute(filter);

    assertEquals(showFound, actualResult);

    verify(showCachePort, times(1)).get(filter.hashCode());
    verify(showCachePort, times(1)).save(filter.hashCode(), showFound);
    verify(showRepositoryPort, only()).getAll();
  }

  @Test
  @DisplayName(
      "When the use case is executed"
          + "with filtering by date (between [2022-02-01 00:00, 2022-03-01 00:00])"
          + "then all performances between that rage should be returned")
  void whenAllShowsAreRequestedFilteringByDateThenThePerformancesInThatRangeShouldBeReturned() {

    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    final var showsBetweenDates =
        List.of(StubsFactory.getFirstShowPerformances(StubsFactory.getFirstPerformance()));

    final var filter =
        ShowFilter.of(
            LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0),
            LocalDateTime.of(2022, Month.MARCH, 1, 0, 0));

    when(showCachePort.get(filter.hashCode())).thenReturn(Optional.empty());
    when(showCachePort.save(filter.hashCode(), showsBetweenDates)).thenReturn(showsBetweenDates);
    when(showRepositoryPort.getAll()).thenReturn(shows);

    final var actualResult = getShowsUseCase.execute(filter);

    assertEquals(showsBetweenDates, actualResult);

    verify(showCachePort, times(1)).get(filter.hashCode());
    verify(showCachePort, times(1)).save(filter.hashCode(), showsBetweenDates);
    verify(showRepositoryPort, only()).getAll();
  }

  @Test
  @DisplayName(
      "When the use case is executed"
          + "with filtering by price (between [40, 100])"
          + "then all performances between that rage should be returned")
  void whenAllShowsAreRequestedFilteringByPriceThenThePerformancesInThatRangeShouldBeReturned() {

    final var filter = ShowFilter.of(BigDecimal.valueOf(40.00), BigDecimal.valueOf(100.00));

    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    final var showsBetweenPrice =
        List.of(
            StubsFactory.getFirstShowPerformances(StubsFactory.getThirdPerformance()),
            StubsFactory.getSecondShowPerformances(StubsFactory.getFourthPerformance()));

    when(showCachePort.get(filter.hashCode())).thenReturn(Optional.empty());
    when(showCachePort.save(filter.hashCode(), showsBetweenPrice)).thenReturn(showsBetweenPrice);
    when(showRepositoryPort.getAll()).thenReturn(shows);

    final var actualResult = getShowsUseCase.execute(filter);

    assertEquals(showsBetweenPrice, actualResult);

    verify(showCachePort, times(1)).get(filter.hashCode());
    verify(showCachePort, times(1)).save(filter.hashCode(), showsBetweenPrice);
    verify(showRepositoryPort, only()).getAll();
  }

  @Test
  @DisplayName(
      "When the use case is executed "
          + "with filtering by date (between [2022-02-01 00:00, 2022-06-01 00:00]) "
          + "and price between [400, 2000] "
          + "then all performances between that rage should be returned")
  void
      whenAllShowsAreRequestedFilteringByDateAndPriceThenThePerformancesInThatRangeShouldBeReturned() {

    final var filter =
        ShowFilter.of(
            LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0),
            LocalDateTime.of(2022, Month.JUNE, 1, 0, 0),
            BigDecimal.valueOf(400.00),
            BigDecimal.valueOf(1000.00));

    final var shows =
        List.of(StubsFactory.getFirstShowPerformances(), StubsFactory.getSecondShowPerformances());

    final var showsBetweenDatesAndPrice =
        List.of(
            StubsFactory.getFirstShowPerformances(StubsFactory.getFirstPerformance()),
            StubsFactory.getSecondShowPerformances(StubsFactory.getSecondPerformance()));

    when(showCachePort.get(filter.hashCode())).thenReturn(Optional.empty());
    when(showCachePort.save(filter.hashCode(), showsBetweenDatesAndPrice))
        .thenReturn(showsBetweenDatesAndPrice);
    when(showRepositoryPort.getAll()).thenReturn(shows);

    final var actualResult = getShowsUseCase.execute(filter);

    assertEquals(showsBetweenDatesAndPrice, actualResult);

    verify(showCachePort, times(1)).get(filter.hashCode());
    verify(showCachePort, times(1)).save(filter.hashCode(), showsBetweenDatesAndPrice);
    verify(showRepositoryPort, only()).getAll();
  }

  @Test
  @DisplayName(
      "When the use case is executed "
          + "with filtering by date (between [2022-02-01 00:00, 2022-06-01 00:00]) "
          + "and price between [40, 2000] "
          + "and ordered by name desc"
          + "then all performances between that rage should be returned")
  void
      whenAllShowsAreRequestedFilteringByDateAndPriceAndOrderedDescThenThePerformancesInThatRangeShouldBeReturned() {

    final var filter =
        ShowFilter.of(
            LocalDateTime.of(2022, Month.JANUARY, 1, 0, 0),
            LocalDateTime.of(2022, Month.JUNE, 1, 0, 0),
            BigDecimal.valueOf(40.00),
            BigDecimal.valueOf(1000.00),
            ShowFilter.Sort.builder()
                .sortByName(true)
                .direction(ShowFilter.Direction.DESC)
                .build());

    final var showsBetweenDatesAndPriceOrderedDescByName =
        new ArrayList<>(
            List.of(
                StubsFactory.getSecondShowPerformances(), StubsFactory.getFirstShowPerformances()));

    when(showCachePort.get(filter.hashCode()))
        .thenReturn(Optional.of(showsBetweenDatesAndPriceOrderedDescByName));

    final var actualResult = getShowsUseCase.execute(filter);

    assertEquals(showsBetweenDatesAndPriceOrderedDescByName, actualResult);

    verify(showCachePort, only()).get(filter.hashCode());
    verify(showCachePort, never()).save(any(), any());
    verify(showRepositoryPort, never()).getAll();
  }
}
