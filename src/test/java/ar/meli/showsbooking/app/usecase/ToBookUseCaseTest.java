package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.exception.ApplicationException;
import ar.meli.showsbooking.app.exception.InfraException;
import ar.meli.showsbooking.app.port.out.BookingRepositoryPort;
import ar.meli.showsbooking.utils.StubsFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("Acceptance criteria for the use case that makes a reservation")
public class ToBookUseCaseTest {

  @Mock BookingRepositoryPort bookingRepositoryPort;
  @InjectMocks ToBookUseCase toBookUseCase;

  @Test
  @DisplayName(
      "When the use case is executed with a valid booking"
          + " then the booking created should be returned")
  void aValidBookingShouldBeCreatedAndReturned() {

    final var newBooking = StubsFactory.newBooking();

    when(bookingRepositoryPort.toBook(newBooking)).thenReturn(newBooking);

    final var actualResult = toBookUseCase.execute(newBooking);

    assertEquals(newBooking, actualResult);

    verify(bookingRepositoryPort, only()).toBook(newBooking);
  }

  @Test
  @DisplayName(
      "When the use case is executed with a valid booking"
          + " and there was an error in the persistence layer"
          + " then a exception must be caught and properly logged")
  void anErrorInThePersistenceShouldBeCaught() {

    when(bookingRepositoryPort.toBook(any())).thenThrow(InfraException.class);

    assertThrows(ApplicationException.class, () -> toBookUseCase.execute(any()));

    verify(bookingRepositoryPort, only()).toBook(any());
  }
}
