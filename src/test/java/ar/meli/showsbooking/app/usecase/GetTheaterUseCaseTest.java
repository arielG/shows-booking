package ar.meli.showsbooking.app.usecase;

import ar.meli.showsbooking.app.port.out.TheaterRepositoryPort;
import ar.meli.showsbooking.utils.StubsFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@DisplayName("Acceptance criteria for the use case that list performances")
class GetTheaterUseCaseTest {

  @InjectMocks GetTheaterUseCase getTheaterUseCase;
  @Mock TheaterRepositoryPort theaterRepositoryPort;

  @Test
  @DisplayName(
      "When the use case is executed, and there is no theater open because the pandemic,"
          + "then should return an empty list")
  void allTheaterClosedShouldReturnAnEmptyList() {

    final var actualResult = getTheaterUseCase.execute();

    assertEquals(List.of(), actualResult);
  }

  @Test
  @DisplayName(
      "When the use case is executed, without any criteria, "
          + "and there is only one theater"
          + "then should return all his programmed performances")
  void onlyOneTheaterOpenThenShouldReturnHisPerformances() {

    final var theater = StubsFactory.getFirstTheater();

    final var expectedResult = List.of(theater);

    when(theaterRepositoryPort.getAll()).thenReturn(List.of(theater));

    final var actualResult = getTheaterUseCase.execute();

    assertEquals(expectedResult, actualResult);
  }

  @Test
  @DisplayName(
      "When the use case is executed, without any criteria, "
          + "and all theaters are open"
          + "then should return all his programmed performances")
  void whenAllTheatersOpenThenReturnAllThePerformances() {

    final var theater1 = StubsFactory.getFirstTheater();
    final var theater2 = StubsFactory.getSecondTheater();

    final var expectedResult = List.of(theater1, theater2);

    when(theaterRepositoryPort.getAll()).thenReturn(List.of(theater1, theater2));

    final var actualResult = getTheaterUseCase.execute();

    assertEquals(expectedResult, actualResult);
  }
}
